# DatePalm.2018
This package describes the phenotypes and metabolic profiles of 196 date fruits. Data exploration for date varieties is also
inducled in the package. The plots can be viewed under Downloads section.

# Installation

To install the package, you first need the
[*devtools*](https://github.com/hadley/devtools) package.

```{r}
install.packages("devtools")
```

Then you can install the *DatePalm.2018* package using

```{r}
library(devtools)
install_bitbucket("shh2026/DatePalm.2018")
```

Install other required packages for running scripts

```{r}
install_bitbucket("graumannlabtools/autonomics.explore")
install_bitbucket("graumannlabtools/autonomics.eset")
install.packages("magrittr")
install.packages("stringi")
```